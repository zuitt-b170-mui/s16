// while loop

let count = 5;

while (count !== 0){
    console.log("While loop: " + count);

    count --;
};

let countTwo = 1;

while(countTwo < 11){
    console.log(countTwo);

    countTwo++;
}

// do-while loop

let countThree = 5;

do{
    console.log("Do-While Loop: " + countThree);

    countThree--;
}while(countThree > 0);

let countFour = 1;

do{
    console.log(countFour);
    countFour++;
}while(countFour < 11);


// for loop

for(let countFive = 5; countFive > 0; countFive --){
    console.log("For Loop: " + countFive)
}

let number = Number(prompt("Give me a Number"));
for(let numCount = 1; numCount <= number; numCount++){
    console.log("Hello Batch 170!");
};

let myName = "alex";
console.log(myName.length);

console.log(myName[2]);

for(let x = 0; x < myName.length; x++){
    console.log(myName[x]);
}

myName = "Alex";
for(let i = 0; i < myName.length; i++){
    if(
        myName[i].toLowerCase() == "a" ||
        myName[i].toLowerCase() == "e" ||
        myName[i].toLowerCase() == "i" ||
        myName[i].toLowerCase() == "o" ||
        myName[i].toLowerCase() == "u" 
    ){
        console.log(3);
    }else{
        console.log(myName[i]);
    }
};

// continue and break statement

for(let countSix = 0; countSix <= 20; countSix++){
    if(countSix % 2 === 0){
        continue;
    }
    console.log("Continue and Break: " + countSix);

    if(countSix > 10){
        break;
    }
}

let name = "Alexandro";

for(let i = 0; i < name.length; i++){
    console.log(name[i]);

    if(name[i].toLowerCase() === "a"){
        console.log("Continue to the next Iteration");
        continue;
    }
    if(name[i].toLowerCase() === "d"){
        break;
    }
}