let var1 = Number(prompt("Enter a number."));
console.log(`The number you provided is ${var1}.`)

for(let varCount = var1; varCount >= 0; varCount--){
    if(varCount > 50 && varCount % 10 == 0){
        console.log("The number is divisible by 10. Skipping the number.");
        continue;
    }
    if(varCount > 50 && varCount % 5 == 0){
        console.log(varCount);
    }
    if(varCount <= 50){
        console.log("The current value is 50. Terminating the loop.");
        break;
    }
};

let var2 = "supercalifragilisticexpialidocious"
let var3 = ""

console.log(var2)

for(let i = 0; i < var2.length; i++){
    if(
        var2[i] == "a" ||
        var2[i] == "e" ||
        var2[i] == "i" ||
        var2[i] == "o" ||
        var2[i] == "u" 
    ){
        continue;
    }else{
        var3 += var2[i];
    }
};

console.log(var3);
